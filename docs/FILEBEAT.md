![Filebeat](images/filebeatlogo.png)
# Filebeat
## Filebeat for API Platform
- For all Akana API Platform nodes (*Community Manager, Policy Manager, Network Director*), install **Filebeat** using RPM (*RedHat 7 was used for this example*) [Instructions](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html)

- Select critical errors to search for. **Filebeat** will tail all logs in `/opt/akana_sw/sm8/instances/*/log` for these phrases: ```        'OutOfMemoryError',
        'Cannot get a connection, pool exhausted',
        'Error in creating Prepared statement for the query',
        'at the below stack trace. Not closed in the same method',
        'org.apache.lucene.store.jdbc.JdbcStoreException',
        'Timeout waiting for idle object',
        'com.digev.fw.exception.GException: javax.wsdl.WSDLException: WSDLException: faultCode=OTHER_ERROR',
        'GC overhead limit exceeded',
        'XmlSiteMinderProviderConfig',
        'Wsdl does not conform to wsdl schema',
        'Error encountered in WS-Security engine',
        'Log frame is closed at the below stack trace',
        'Log block not closed correctly. Enable log block tracking to see diagnostic information',
        'No corresponding startTraceBlock() is seen',
        'Illegal character',
        'com.mysql.jdbc.exceptions.jdbc4.CommunicationsException',
        'Unable to initialize SiteMinder agent',
        'Data truncation',
        'federation member auth token cannot be refreshed',
        'org.compass.core.engine.SearchEngineException: Failed to process job',
        'UsageJDBCWriter.writeUsage',
        '/openid/login',
        'java.lang.NullPointerException at com.soa.jbi.component.http.marshal.impl.OutgoingExchangeInitializer',
	'ERROR [DBStatementAndResultSetTracker] PreparedStatementTracker',
        'No Subject is associated with the call. Only Container identities can invoke this call. Returning authorization error',
	'com.digev.fw.exception.GException: No process found',
        'Log block not closed correctly. Enable log block tracking to see diagnostic information' ```

- **Filebeat** is written in GO so we will need to translate the above phrases to single [regex expression for GO](https://support.google.com/analytics/answer/1034324?hl=en). This is probably not the most ideal way of handling errors, but we do not want to send verbose error logs to syslog server either. The final regex looks something like this:
```
'(\W|^)OutOfMemoryError(\W|$)','(\W|^)Cannot\sget\sa\sconnection\spool\sexhausted(\W|$)','(\W|^)Error\sin\screating\sPrepared\sstatement\sfor\sthe\squery(\W|$)','(\W|^)at\sthe\sbelow\sstack\strace.\sNot\sclosed\sin\sthe\ssame\smethod(\W|$)','(\W|^)org\sapache\slucene\sstore\sjdbc\sJdbcStoreException(\W|$)','(\W|^)Timeout\swaiting\sfor\sidle\sobject(\W|$)','(\W|^)com\sdigev\sfw\sexception\sGException\sjavax\swsdl\sWSDLException\sWSDLException\sfaultCode\sOTHER_ERROR(\W|$)','(\W|^)GC\soverhead\slimit\sexceeded(\W|$)','(\W|^)XmlSiteMinderProviderConfig(\W|$)','(\W|^)Wsdl\sdoes\snot\sconform\sto\swsdl\sschema(\W|$)','(\W|^)Error\sencountered\sin\sWS-Security\sengine(\W|$)','(\W|^)Log\sframe\sis\sclosed\sat\sthe\sbelow\sstack\strace(\W|$)','(\W|^)Log\sblock\snot\sclosed\scorrectly\sEnable\slog\sblock\stracking\sto\ssee\sdiagnostic\sinformation(\W|$)','(\W|^)No\scorresponding\sstartTraceBlock\sis\sseen(\W|$)','(\W|^)Illegal\scharacter(\W|$)','(\W|^)com\smysql\sjdbc\sexceptions\sjdbc4\sCommunicationsException(\W|$)','(\W|^)Unable\sto\sinitialize\sSiteMinder\sagent(\W|$)','(\W|^)Data\struncation(\W|$)','(\W|^)federation\smember\sauth\stoken\scannot\sbe\srefreshed(\W|$)','(\W|^)org\scompass\score\sengine\sSearchEngineException\sFailed\sto\sprocess\sjob(\W|$)','(\W|^)UsageJDBCWriter\swriteUsage(\W|$)','(\W|^)java\slang\sNullPointerException\sat\scom\ssoa\sjbi\scomponent\shttp\smarshal\simpl\sOutgoingExchangeInitializer(\W|$)','(\W|^)ERROR\sDBStatementAndResultSetTracker\sPreparedStatementTracker(\W|$)','(\W|^)No\sSubject\sis\sassociated\swith\sthe\scall\sOnly\sContainer\sidentities\scan\sinvoke\sthis\scall\sReturning\sauthorization\serror(\W|$)','(\W|^)com\sdigev\sfw\sexception\sGException\sNo\sprocess\sfound(\W|$)','(\W|^)Log\sblock\snot\sclosed\scorrectly\sEnable\slog\sblock\stracking\sto\ssee\sdiagnostic\sinformation(\W|$)'
```

- Use the **Filebeat** configuration file below and `/etc/filebeat/filebeat.yml`. Ignore the **Logstash** output for now.

```
filebeat.prospectors:
- input_type: log
  paths:
    - /opt/akana_sw/sm8/instances/*/log/*.log

  include_lines:[*copy and paste the regex expression above here*]

output.logstash:
  hosts: ["es3.apex.dev:5044"]
```

- Start **Filebeat** service `service filebeat start`

## Filebeat for MySQL
- Install **Filebeat** using RPM on MySQL node.

- We need to load [MySQL module](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-module-mysql.html) when the service is started. Edit `/etc/init.d/filebeat` and change `args` to the following:

```
-e -modules=mysql -c /etc/filebeat/filebeat.yml -path.home /usr/share/filebeat -path.config /etc/filebeat -path.data /var/lib/filebeat -path.logs /var/log/filebeat```. Save then reload the service `systemctl daemon-reload`.
```

3. MySQL module allows monitoring for application logs as well as slow query logs. We will only configure slow query logs (*queries slower than 10 seconds*) for MySQL database that Akana Platform is using. MySQL configuration file (`/etc/my.cnf`) has the following to detect and output slow queries:

```
log-error = /mysql/mysql-error.log
log-queries-not-using-indexes = 1
slow-query-log = 1
log_queries_not_using_indexes = 0
slow-query-log-file = /mysql/mysql-slow.log
long_query_time = 10
```

4. Replace **Filebeat** configuration file `/etc/filebeat/filebeat.yml` with the following (*note: Logstash output is using a different port than Akana's setup above*)

```
filebeat:
  # List of prospectors to fetch data.
  prospectors:
  # Each - is a prospector. Below are the prospector specific configurations
  # Paths that should be crawled and fetched. Glob based paths.
  # To fetch all ".log" files from a specific level of subdirectories
  # /var/log/*/*.log can be used.
  # For each file found under this path, a harvester is started.
  # Make sure not file is defined twice as this can lead to unexpected behaviour.
    - paths:
        - /mysql/mysql-slow.log
      input_type: log
      document_type: mysql
      fields: { log_type: query }
      mysql_version: 5.6
      fields_under_root: true
      multiline:
        pattern: '(^# Time:.*)?^# User@Host:.*'
        negate: true
        match: after

  # Name of the registry file. Per default it is put in the current working
  # directory. In case the working directory is changed after when running
  # filebeat again, indexing starts from the beginning again.
  # registry_file: /var/lib/filebeat/registry
############################# Output ##########################################
# Configure what outputs to use when sending the data collected by the beat.
# Multiple outputs may be used.
output:
  ### Logstash as output
  logstash:
    # The Logstash hosts
    hosts: ["es3.apex.dev:5044"]

logging.level: error
logging.to_files: true
logging.files.path: /var/log/filebeat
logging.files.name: filebeat.log
logging.files.rotateeverybytes: 10485760
logging.files.keepfiles: 7
```

- Start **Filebeat** service `service filebeat start`

[Go back to README.md](README.md)
