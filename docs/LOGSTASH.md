![Logstash](images/logstashlogo.png)
# Logstash
## Install Logstash / Elasticsearch / Kibana
- [Install Logstash using RPM](https://www.elastic.co/guide/en/logstash/current/installing-logstash.html)

- [Install Elasticsearch using TAR *OPTIONAL*](https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html)

- Install NGINX for Kibana *OPTIONAL*

- [Install Kibana using RPM *OPTIONAL*](https://www.elastic.co/guide/en/kibana/current/rpm.html)

- [Install following plugins for Logstash:](https://www.elastic.co/guide/en/logstash/current/working-with-plugins.html)
```
logstash-codec-multiline
logstash-codec-plain
logstash-filter-grok
logstash-filter-multiline
logstash-filter-mutate
logstash-filter-syslog_pri
logstash-input-beats
logstash-input-jdbc
logstash-output-elasticsearch
logstash-output-syslog
```

- Create a directory to store input/output/filtering rules (e.g. `/etc/logstash/conf.d`)

- Create a configuration for Filebeat input from Akana nodes with output for both Elasticsearch and Log Insight (syslog). Save the file (e.g. `akana-beats-input-es-output.conf`) to the directory above. Example:

```
# Logstash input, output, filter
# Written by Bryan Kwon
# June 27th, 2017

input {
  #Akana: file streams from PM,CM, and ND logs
  beats {
    port => 5044
    type => "log"
    tags => [ "akana","api_gateway","error_logs" ]
  }
}
filter {
  if "akana" in [tags] {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
      add_field => { "temp_message" => "temp message" }
    }

    mutate {
      copy => { "message" => "temp_message" }
    }

    mutate {
      replace => { "message" => "Critical alert raised on Akana Platform: %{host}. Error is: %{temp_message}" }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
}
output {
        elasticsearch { hosts => ["localhost:9200"] }
        syslog {
                host => "192.168.130.9"
                port => 514
        }
}
```

- Create a configuration for Filebeat input from MySQL node (slow query) with output for both Elasticsearch and Log Insight (syslog). Save the file (e.g. `mysql-slowquery-input-syslog-output.conf`) to the directory above. Example:

```
input {
  beats {
    port => 5055
    tags => [ "akana","mysql","slow_query" ]
    }
}
filter {
  multiline{
      pattern => "(^# Time:.*)?^# User@Host:.*"
      negate => true
      what => previous
  }

  grok { match => [ "message", "%{MYSQLTIME}" ] }
  if ("_grokparsefailure" in [tags]) {
      mutate {
          remove_tag => [ "_grokparsefailure" ]
      }
  } else {
      drop { }
  }
  grok { match => [ "message", "%{MYSQLHOST}" ] }
  if ("_grokparsefailure" in [tags]) {
      mutate {
          remove_tag => [ "_grokparsefailure" ]
      }
  }
  grok { match => [ "message", "%{MYSQLSTATS}"] }
  if ("_grokparsefailure" in [tags]) {
      mutate {
          remove_tag => [ "_grokparsefailure" ]
      }
  }
  grok { match => [ "message", "%{MYSQLBYTES}"] }
  if ("_grokparsefailure" in [tags]) {
      mutate {
          remove_tag => [ "_grokparsefailure" ]
      }
  }
  grok { match => [ "message", "%{MYSQLUSEDB}"] }
  if ("_grokparsefailure" in [tags]) {
      mutate {
          remove_tag => [ "_grokparsefailure" ]
      }
  }
  grok { match => [ "message", "%{MYSQLQUERY}" ] }
  date {
      match => [ "timestamp", "UNIX" ]
  }
}
output {
        elasticsearch { hosts => ["localhost:9200"] }
        syslog {
                host => "192.168.130.9"
                port => 514
        }
}
```

- The last configuration file will make Logstash call MySQL to monitor Akana tables every 5 minutes. Create a configuration as shown below. Save the file (e.g. `mysql-input-es-output.conf`) to the directory above. Example:

```
input {
  # Akana: Check the outgoing email queue. If the queue size is more than 10, raise alert.
  # runs every 5 mins
  jdbc {
    statement => "select count(*) from MSG_QUEUE"
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","msg_queue"]
    schedule => "*/5 * * * *"
    clean_run => true
  }

  # Akana: Check the index queue. If the queue size is more than 100, raise alert.
  jdbc {
    statement => "select count(*) from INDEX_QUEUE"
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","index_queue" ]
    schedule => "*/5 * * * *"
    clean_run => true
  }

  # Akana: There are blocked jobs in mysql
  jdbc {
    statement => "select * from SOA_QRTZ_TRIGGERS where TRIGGER_STATE = 'BLOCKED' and TRIGGER_NAME not in (select TRIGGER_NAME from SOA_QRTZ_FIRED_TRIGGERS)"
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","blocked_triggers" ]
    schedule => "*/5 * * * *"
    clean_run => true
  }

  # Akana: There are acquired jobs in mysql
  jdbc {
    statement => "select * from SOA_QRTZ_TRIGGERS where TRIGGER_STATE = 'ACQUIRED' and TRIGGER_NAME not in (select TRIGGER_NAME from SOA_QRTZ_FIRED_TRIGGERS)"
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","acquired_triggers" ]
    schedule => "*/5 * * * *"
    clean_run => true
  }

  # Akana: Check how many send mail attempts were made
  # runs every 5 mins
  jdbc {
    statement => "select max(DELIVERYATTEMPTS) from MSG_QUEUE"
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","msg_delivery_attempts"]
    schedule => "*/5 * * * *"
    use_column_value => true
    tracking_column => DELIVERYATTEMPTS
    tracking_column_type => numeric
    clean_run => true
  }

  # Akana: Database job running for over 20 minutes
  # runs every 5 mins
  jdbc {
    statement => 'select TRIGGER_NAME, STATE, INSTANCE_NAME, FROM_UNIXTIME(FIRED_TIME/1000), "GMT" from SOA_QRTZ_FIRED_TRIGGERS where FIRED_TIME/1000 <= UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE, -20, now()))'
    jdbc_connection_string => "jdbc:mysql://db.apex.dev:3306/pm82"
    jdbc_user => "akana_rw"
    jdbc_password => "!QAZ2wsx"
    jdbc_driver_library => "/etc/logstash/mysql-connector-java-5.1.39-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    tags => [ "akana","monitoring","database_queue_check","long_running_job"]
    schedule => "*/5 * * * *"
    clean_run => true
  }
}
filter {
  if "msg_queue" in [tags] and [count(*)] > 10 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: Outgoing message queue size is greater than 10. Symptom: SMTP not processing emails or resource issue on CM or PM."
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
  if "index_queue" in [tags] and [count(*)] > 100 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: Index queue size is greater than 100. Symptom: Resource issue on CM or Elasticsearch."
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
  if "blocked_triggers" in [tags] and [count(*)] > 0 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: There are blocked triggers. Symptom: Possible database table lock."
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
  if "acquired_triggers" in [tags] and [count(*)] > 0 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: There are acquired triggers. Symptom: Possible database table lock or long running job"
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
  if "msg_delivery_attempts" in [tags] and [max(deliveryattempts)] > 3 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: Email delivery attempt is more than 3. Symptom: SMTP down"
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
  if "long_running_job" in [tags] and [count(*)] > 0 {
    syslog_pri {
      facility_labels => "log alert"
      severity_labels => "critical"
    }
    mutate {
      replace => {
        "message" => "Critical alert raised on Akana Platform: Scheduled Query Job for Akana Database. Error: Scheduled Job has been running for more than 20 minutes Symptom: Please check CM or PM."
      }
      add_field => {
        "appname" => "akana"
        "severity" => "critical"
      }
    }
  }
}
output {
    elasticsearch { hosts => ["localhost:9200"] }
    syslog {
              host => "192.168.130.9"
              port => 514
    }
}
```

- Start Logstash service `service logstash start`

[Go back to README.md](README.md)
