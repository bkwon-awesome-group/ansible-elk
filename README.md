![Roguewave](images/roguewavelogo.png)
![Elastic](images/elasticlogo.png)
# Monitoring Akana API Platform with ELK
## Summary
This repository contains Ansible script with configuration files for deploying ELK stack. Instructions below are for configuring [Logstash 5.5](https://www.elastic.co/guide/en/logstash/current/index.html) and [Filebeat 5.5](https://www.elastic.co/guide/en/beats/filebeat/current/index.html) to push critical application alerts to a syslog server - [vRealize Log Insight 3.3](Error in sending Email Alert). If you wish to jump straight to Ansible deployment instructions, you can check out [Ansible deployment instructions](docs/ANSIBLE.md).

## Components
| Product | Version | Description |
| :--- | :---: | :--- |
| **Akana API Platform** | `v8.4.3` | *Community Manager, Policy Manager, Network Diretor* |
| **Logstash** | `v5.5` | Log filter - massages the raw data feed from *Filebeat* and sends to *Log Insight* |
| **Filebeat** | `v5.5` | Log pusher - tails application log files and *pushes* to *Logstash* |
| **vRealize Log Insight** | `v3.3` | Syslog server |
| *(optional)* **Elasticsearch** | `v5.5` | Analytic engine - provides more analytic options than *Log Insight* |
| *(optional)* **Kibana** | `5.5` | Visualization engine - display analytic data from *Elasticsearch* in meaningful way |

## Getting Started
1. [Install Filebeat on Akana Platform and MySQL nodes](docs/FILEBEAT.md)
2. [Install Logstash](docs/LOGSTASH.md)

## Testing
### Basics
Filebeat configuration has two settings: `prospector` and `output`. `Prospector` configures log file location and type of gathering to be had on the log files (e.g. log vs syslog). `Output` configures where file output should forward to.
> NOTE: You can define more than one `prospector` and `output`

Logstash configuration has three settings: `input`, `filter`, `output`. `Input` receives from various sources (e.g. syslog, tcp, file, filebeat, and etc). You can use `filter` to modify the `input` feed into desirable `output` feed. When using multiple configuration files, make sure each input/filter/output are distinct. For instance, `type` and `input_type` variables are used in my example to segregate each feed.

### MySQL slow logs
Logstash configuration file: `/etc/logstash/conf.d/mysql-slowquery-input-syslog-output.conf`

Changing `long_query_time = 10` to `long_query_time = .1` on `/etc/my.cnf` gives us enough chatter in `mysql-slow.log` for Filebeat to output to Logstash. `Multiline` and `grok` filter plugin are used to massage the feed into presentable `message`. `Syslog` output filtering will add `sourcehost`, `procid`, and other syslog details on top of the `message` and ships them to the syslog server (`Log Insight`).

*Sample screenshot:*
![Log Insight screenshot](images/mysql_slow_query.png)

### MySQL scheduled queries
Logstash configuration file: `/etc/logstash/conf.d/mysql-input-es-output.conf`

Logstash `input` is multiple jdbc callout to MySQL database for Akana. Each query result is checked by the `filter` then sent to `output` if alert criteria has been met. Below syslog alert is raised when there are outgoing email messages stuck in the MSG_QUEUE table.

*Sample screenshot:*
![Log Insight screenshot](images/mysql_scheduled_query.png)

### Akana Platform application logs
Logstash configuration file: `/etc/logstash/conf.d/akana-beats-input-es-output.conf`

Restarting MySQL gives us critical alert `input` into Logstash.

*Sample screenshot:*
![Log Insight screenshot](images/akana_error_log.png)
